package ru.t1.strelcov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getMQConnectionFactory();

    @NotNull
    String getMongoHost();

    @NotNull
    Integer getMongoPort();

}
