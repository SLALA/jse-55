package ru.t1.strelcov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void writeLog(@NotNull String json);

}
