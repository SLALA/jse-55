package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String CONFIG_FILE = "application.properties";

    @NotNull
    private static final String MQ_CONNECTION_FACTORY = "mq.connection.factory";

    @NotNull
    public static final String MQ_CONNECTION_FACTORY_DEFAULT = "tcp://localhost:61616";

    @NotNull
    private static final String MONGO_HOST = "mongo.host";

    @NotNull
    public static final String MONGO_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String MONGO_PORT = "mongo.port";

    @NotNull
    public static final String MONGO_PORT_DEFAULT = "27017";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream(CONFIG_FILE);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getPropertyValue(@NotNull String key, @NotNull String defaultValue) {
        if (System.getenv().containsKey(key)) return System.getenv(key);
        if (System.getProperties().containsKey(key)) return System.getProperty(key);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private Integer getPropertyIntValue(@NotNull String key, @NotNull String defaultValue) {
        @NotNull final String value;
        if (System.getenv().containsKey(key)) value = System.getenv(key);
        else if (System.getProperties().containsKey(key)) value = System.getProperty(key);
        else value = properties.getProperty(key, defaultValue);
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getMQConnectionFactory() {
        return getPropertyValue(MQ_CONNECTION_FACTORY, MQ_CONNECTION_FACTORY_DEFAULT);
    }

    @Override
    public @NotNull String getMongoHost() {
        return getPropertyValue(MONGO_HOST, MONGO_HOST_DEFAULT);
    }

    @Override
    public @NotNull Integer getMongoPort() {
        return getPropertyIntValue(MONGO_PORT, MONGO_PORT_DEFAULT);
    }

}