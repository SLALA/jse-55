package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IJmsSenderService {

    void send(@NotNull String json);

}
