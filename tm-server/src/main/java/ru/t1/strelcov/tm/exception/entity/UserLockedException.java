package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UserLockedException extends AbstractException {

    public UserLockedException() {
        super("Error: User is locked.");
    }

}
