package ru.t1.strelcov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO removeByLogin(@NotNull String login);

    boolean loginExists(@NotNull final String login);

}
