package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UserLoginExpiredException extends AbstractException {

    public UserLoginExpiredException() {
        super("Error: Your session is expired. Login again.");
    }

}
