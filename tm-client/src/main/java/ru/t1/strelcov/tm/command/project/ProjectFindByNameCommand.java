package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectFindByNameRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-find-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Find project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[FIND PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.findByNameProject(new ProjectFindByNameRequest(getToken(), name)).getProject();
        showProject(project);
    }

}
