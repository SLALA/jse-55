package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectChangeStatusByNameRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.IN_PROGRESS;

@Component
public final class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.changeStatusByNameProject(new ProjectChangeStatusByNameRequest(getToken(), name, IN_PROGRESS.name())).getProject();
        showProject(project);
    }

}
