package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class DisplayHelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = commandService.getCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

}
