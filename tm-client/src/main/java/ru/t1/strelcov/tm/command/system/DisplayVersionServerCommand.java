package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;

@Component
public final class DisplayVersionServerCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display server version info.";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER VERSION]");
        System.out.println(systemEndpoint.getVersion(new ServerVersionRequest()).getVersion());
    }

}
