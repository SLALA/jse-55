package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class DisplayCommands extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Display commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = commandService.getCommands();
        for (final AbstractCommand command : commands) {
            @NotNull final String name = command.name();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
