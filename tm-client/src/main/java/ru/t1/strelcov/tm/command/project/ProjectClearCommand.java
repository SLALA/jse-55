package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.ProjectClearRequest;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        projectEndpoint.clearProject(new ProjectClearRequest(getToken()));
    }

}
