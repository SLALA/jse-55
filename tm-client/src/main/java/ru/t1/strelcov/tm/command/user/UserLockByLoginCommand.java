package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.UserLockByLoginRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class UserLockByLoginCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        userEndpoint.lockByLogin(new UserLockByLoginRequest(getToken(), login));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
