package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskUpdateByNameRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String oldName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.updateByNameTask(new TaskUpdateByNameRequest(getToken(), oldName, name, description)).getTask();
        showTask(task);
    }

}
