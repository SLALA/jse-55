package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectFindByIdRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class ProjectFindByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-find-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Find project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[FIND PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.findByIdProject(new ProjectFindByIdRequest(getToken(), id)).getProject();
        showProject(project);
    }

}
