package ru.t1.strelcov.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.strelcov.tm.api.endpoint.*;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.ITokenService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;

import java.util.Optional;

@Setter
public abstract class AbstractCommand implements ITokenService {

    @Autowired
    @NotNull
    protected IAuthEndpoint authEndpoint;

    @Autowired
    @NotNull
    protected ISystemEndpoint systemEndpoint;

    @Autowired
    @NotNull
    protected IDataEndpoint dataEndpoint;

    @Autowired
    @NotNull
    protected IProjectEndpoint projectEndpoint;

    @Autowired
    @NotNull
    protected ITaskEndpoint taskEndpoint;

    @Autowired
    @NotNull
    protected IUserEndpoint userEndpoint;

    @Autowired
    @NotNull
    protected IPropertyService propertyService;

    @Autowired
    @NotNull
    protected ICommandService commandService;

    @Autowired
    @NotNull
    private ITokenService tokenService;

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @NotNull final String name = name();
        final String arg = arg();
        final String description = description();
        if (!name.isEmpty()) result += name + " ";
        if (arg != null && !arg.isEmpty()) result += "[" + arg + "] ";
        if (!description.isEmpty()) result += "- " + description;
        return result;
    }

    @NotNull
    @Override
    public String getToken() {
        @Nullable final String token = tokenService.getToken();
        Optional.ofNullable(token).filter((i) -> !i.isEmpty()).orElseThrow(AccessDeniedException::new);
        return token;
    }

    @Override
    public void setToken(@NotNull final String token) {
        tokenService.setToken(token);
    }

}
