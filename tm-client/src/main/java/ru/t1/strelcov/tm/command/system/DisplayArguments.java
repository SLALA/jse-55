package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class DisplayArguments extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Display arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getArguments();
        for (final AbstractCommand command : commands) {
            @Nullable final String name = command.arg();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
