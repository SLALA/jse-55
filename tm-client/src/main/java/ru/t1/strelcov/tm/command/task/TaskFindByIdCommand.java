package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskFindByIdRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class TaskFindByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-find-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Find task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.findByIdTask(new TaskFindByIdRequest(getToken(), id)).getTask();
        showTask(task);
    }

}
