package ru.t1.strelcov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface ITaskEndpoint {

    @NotNull String SPACE = "http://endpoint.tm.strelcov.t1.ru/";
    @NotNull String PART = "TaskEndpointService";
    @NotNull String NAME = "TaskEndpoint";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdlURL);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(ITaskEndpoint.class);
    }

    @WebMethod
    @NotNull
    TaskListResponse listTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListRequest request);

    @WebMethod
    @NotNull
    TaskListSortedResponse listSortedTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListSortedRequest request);

    @WebMethod
    @NotNull
    TaskListByProjectIdResponse listByProjectIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListByProjectIdRequest request);

    @WebMethod
    @NotNull
    TaskChangeStatusByIdResponse changeStatusByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskChangeStatusByIdRequest request);

    @WebMethod
    @NotNull
    TaskChangeStatusByNameResponse changeStatusByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskChangeStatusByNameRequest request);

    @WebMethod
    @NotNull
    TaskClearResponse clearTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskClearRequest request);

    @WebMethod
    @NotNull
    TaskCreateResponse createTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskCreateRequest request);

    @WebMethod
    @NotNull
    TaskFindByIdResponse findByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskFindByIdRequest request);

    @WebMethod
    @NotNull
    TaskFindByNameResponse findByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskFindByNameRequest request);

    @WebMethod
    @NotNull
    TaskRemoveByIdResponse removeByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskRemoveByIdRequest request);

    @WebMethod
    @NotNull
    TaskRemoveByNameResponse removeByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskRemoveByNameRequest request);

    @WebMethod
    @NotNull
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskBindToProjectRequest request);

    @WebMethod
    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUnbindFromProjectRequest request);

    @WebMethod
    @NotNull
    TaskUpdateByIdResponse updateByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUpdateByIdRequest request);

    @WebMethod
    @NotNull
    TaskUpdateByNameResponse updateByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUpdateByNameRequest request);

}
