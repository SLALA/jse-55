package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class TaskUpdateByNameRequest extends AbstractUserRequest {

    @Nullable
    private String description;

    @Nullable
    private String name;

    @Nullable
    private String oldName;

    public TaskUpdateByNameRequest(@Nullable final String token, @Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        super(token);
        this.oldName = oldName;
        this.name = name;
        this.description = description;
    }

}
